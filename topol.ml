(* 
    Author: Tomasz Necio
    Code review: Tomasz Stelągowski

    This implementation uses depth-first search algorithm.
    More about this algorithm can be read here:
    https://en.wikipedia.org/wiki/Topological_sorting#Depth-first_search
    
    This implementation requires PMap module.
   
    Expected time complexity is O((E + V) * log(V)), where
        E - number of edges of the directed acyclic graph
        V - number of vertices,
    becuase we travel through each edge exactly once, and every time we hit a
    vertice we make a constant number of calls to functions from PMap module 
    which take logarithmic time (we use maps with vertices of the graph as
    their domain, hence log(V)).
*)
   

exception Cykliczne

type mark = Not_visited | Current_path | Visited

let create_map l =
    (* 
        this function creates a map from vertices to pairs
        (state of the graph traversing function : mark,
         list of child nodes : 'a list)
        The first element tells us whether we have already been in the node,
        and is essential for the algorithm. The second element creates a new
        representation of the graph. Storing information in this way is better
        than going through the input list every time we need to do something
        with the child nodes (log(n) vs n).
    *)
    let map_adder m (a, bl) =
        (PMap.add a (Not_visited, bl) m)
    in List.fold_left map_adder (PMap.empty) l 

let rec visit (m, sorted) node =
    (* this is the main function that traverses the graph *)
    
    let m, (node_status, children) = (
        (* 
            some nodes with no children may not appear as the first element of
            a pair in the input list; therefore they don't appear in our map.
            Here we catch them as soon as they appear and add the to the map.
        *)
        try
            m, (PMap.find node m)
        with
        | Not_found ->
            (PMap.add node (Not_visited, []) m),
            (Not_visited, [])
    ) in 
    
    match node_status with
    | Current_path -> 
        (* we always go from a parent to a child, so if we reappear at the
        same place it means we are going in a cycle *)
        raise Cykliczne
    | Visited -> 
        (* if this node has already been processed in an earlier iteration of
        the algorithm we escape *)
        (m, sorted)
    | Not_visited -> 
        let m = 
            (* we mark this place this way so if we return here we know we
            are going in cycles *)
            (PMap.add node (Current_path, children) m)
        in let (m, sorted) =
            (* here we go deeper down the graph *)
            List.fold_left visit (m, sorted) children
        in 
            (* After we traversed everything that was below, we glue
            this node to the rest of already sorted nodes and close
            this path *)
            (PMap.add node (Visited, children) m, node::sorted)

let rec toposort l m acc =
    (* main sorting function *)        
    match l with 
    (* we go through the list looking for starting nodes, and after we go
       through them all we return the accumulated sorted list *)
    | [] -> acc
    | (h1, _)::t ->
        if (fst (PMap.find h1 m)) = Not_visited then
            let (m, acc) = visit (m, acc) h1 in
            toposort t m acc
        else
            toposort t m acc

let topol l =
    let m = create_map l in
    toposort l m []
