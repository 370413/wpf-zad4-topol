let l = 
    [("F", ["G"; "H"; "I"]);
     ("B", ["C"; "E"; "I"]);
     ("C", ["G"; "D"; "E"; "I"]);
     ("G", ["H"]);
     ("H", ["I"]);
     ("A", ["B"; "D"]);
     ("D", ["E"]);
     ("E", ["F"])];;
assert ((topol l) = ["A"; "B"; "C"; "D"; "E"; "F"; "G"; "H"; "I"]);;

try
    topol [(1, [2]); (2, [1])]
with
    | Cykliczne -> 
        print_string "ok";
        [] ;;

let l = 
    [("a", ["b"; "c"]);
     ("d", ["e"; "f"])];;

topol l;; (* multiple correct outputs *)
